#!/bin/sh
#SBATCH --mail-user={email}
#SBATCH --mail-type=ALL
#SBATCH --qos={queue}
##SBATCH --account={queue}
#SBATCH -p {partition}
#SBATCH --cpus-per-task={threads}
#SBATCH --mem={memory}
#SBATCH --job-name={jobname}
#SBATCH -o {stdout}
#SBATCH --array=1-{num}
### Import your modules
# novocraft
module add java/1.8.0 graalvm/ce-java8-20.0.0 R/3.6.2 Python/2.7.17 bowtie2/2.3.5.1 bwa/0.7.17 samtools/1.13 GenomeAnalysisTK/4.1.9.0 picard-tools/2.23.3 htslib/1.13

### INPUT FILES
{input}

### INPUT FILE BASENAME
BASE=`basename ${{INFILE_1%{ext}}}`

### OUTPUT DIRECTORY
OUTDIR={outdir}

### FUNCTIONS
function timer()
{{
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local  stime=$1
        etime=$(date '+%s')
        if [[ -z '$stime' ]]; then stime=$etime; fi
        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%d:%02d:%02d' $dh $dm $ds
    fi
}}

###PROGRAMS
#export LD_LIBRARY_PATH=/pasteur/projets/policy02/Matrix/metagenomics/htslib/lib:$LD_LIBRARY_PATH
export PYTHONPATH=/pasteur/zeus/projets/p02/Matrix/metagenomics/python-lib/lib/python2.7/site-packages/:$PYTHONPATH

echo $date
echo "Mapping files: $INFILE_1 & $INFILE_2"
start_time=$(timer)
echo "Started at " $(date +"%T")

start_time_{map_tool}=$(timer)
echo "Mapping with {map_tool} started at $(date +"%T")"
{map_cmd}
echo "Elapsed time with {map_tool} : $(timer $start_time_{map_tool})"

start_time_{count_tool}=$(timer)
echo "Mapping analysis with {count_tool} started at $(date +"%T")"
python {script_loc}/counting.py $OUTDIR/sam/${{BASE}}.sam \
$OUTDIR/comptage/${{BASE}}.txt --{count_tool} {bam} -m {memory} -t {threads}
echo "Elapsed time with {count_tool} : $(timer $start_time_{count_tool})"
# if variant calling mode
{variant}
echo "Total duration :  $(timer $start_time)"
